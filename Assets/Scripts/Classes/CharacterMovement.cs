﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {
	private GameObject playerSprite;
	private Rigidbody2D playerRigidBody2D;
	private Animator anim;
	private float movePlayerVector;
	private bool facingRight;
	public float speed = 4.0f;
	public float jumpHeight = 2.0f;
	public float jumpTime = 0.4f;
	public bool jumping = false;
	public float jumpTimer = 0.0f;

	void Awake () {
		playerRigidBody2D = gameObject.GetComponent<Rigidbody2D>();
		playerSprite = transform.Find ("PlayerSprite").gameObject;
		anim = playerSprite.GetComponent<Animator>();
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		movePlayerVector = Input.GetAxis("Horizontal");

		anim.SetFloat("speed", Mathf.Abs(movePlayerVector));

		playerRigidBody2D.velocity = new Vector2(movePlayerVector * speed, playerRigidBody2D.velocity.y);

		if(movePlayerVector > 0 && !facingRight) {
			Flip();
		} else if (movePlayerVector < 0 && facingRight) {
			Flip();
		}

		ProcessJump();

	}

	void Flip () {
		facingRight = !facingRight;
		Vector3 theScale = playerSprite.transform.localScale;
		theScale.x *= -1;
		playerSprite.transform.localScale = theScale;
	}

	void ProcessJump () {
		if(Input.GetButtonDown("Jump") && !jumping) {
			jumping = true;
			anim.SetBool("jumping", true);
		}

		if (jumping) {
			jumpTimer += Time.deltaTime;
			if (jumpTimer > jumpTime) {
				jumpTimer = 0;
				jumping = false;
				anim.SetBool("jumping", false);
			}
		}
	}
}
