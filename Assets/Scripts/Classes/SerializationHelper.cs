﻿using System.IO;
using System.Xml.Serialization;

public class SerializationHelper {
	public static byte[] Serialize<T> (T input) {
		byte[] output = null;
		var serializer = new XmlSerializer(typeof(T));
		try {
			using (var stream = new MemoryStream()) {
				serializer.Serialize(stream, input);
				output = stream.GetBuffer();
			}
		} catch { }
		
		return output;
	}
	
	public static T DeSerialize<T> (Stream input) {
		T output = default(T);
		var serializer = new XmlSerializer(typeof(T));
		try {
			output = (T) serializer.Deserialize(input);
		} catch { }
		return output;
	}
}
