﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;

public static class GameState {
	public static Player CurrentPlayer = ScriptableObject.CreateInstance<Player>();
	public static bool PlayerReturningHome;
	public static Dictionary<string, Vector3> LastScenePositions = new Dictionary<string, Vector3> ();
	static string saveFilePath = Application.persistentDataPath + "/playerstate.dat";

	public static Vector3 GetLastScenePosition (string sceneName) {
		if (GameState.LastScenePositions.ContainsKey(sceneName)) {
			var lastPos = GameState.LastScenePositions[sceneName];
			return lastPos;
		} else {
			return Vector3.zero;
		}
	}

	public static void SetLastScenePosition (string sceneName, Vector3 position) {
		if (GameState.LastScenePositions.ContainsKey(sceneName)) {
			GameState.LastScenePositions[sceneName] = position;
		} else {
			GameState.LastScenePositions.Add (sceneName, position);
		}
	}
	
	public static void SaveState () {
		try {
			PlayerPrefs.SetString("CurrentLocation", Application.loadedLevelName);
			using (var file = File.Create(saveFilePath)){
				var playerSerializedState = SerializationHelper.Serialize<PlayerSaveState>(CurrentPlayer.GetPlayerSaveState());
				file.Write(playerSerializedState, 0, playerSerializedState.Length);	
			}
		} catch {
			Debug.LogError ("Saving data failed");
		}
	}
	
	public static bool SaveAvaiable {
		get {return File.Exists(saveFilePath); }
	}
	
	public static void LoadState (Action LoadComplete) {
		try {
			if (SaveAvaiable) {
				using (var stream = File.Open (saveFilePath, FileMode.Open)) {
					var LoadedPlayer = SerializationHelper.DeSerialize<PlayerSaveState>(stream);
					CurrentPlayer = LoadedPlayer.LoadPlayerSaveState(CurrentPlayer);
				}
			}
		} catch {
			Debug.LogError ( "Loading data failed, file is corrupt");
		}
		 LoadComplete();
	}
}
