﻿using UnityEngine;
using System.Collections;

public class ConversationManager : Singleton<ConversationManager> {
	bool talking = false;
	bool buildingText = false;
	ConversationEntry currentConversationLine;
	string currentConversationText;
	float textSpeed = 0.05f;
	float maxTextSpeed = 1.0f;
	int fontSpacing = 7;
	int conversationTextWidth;
	int dialogHeight = 70;
	public int displayTextureOffset = 70;
	Rect scaledTextureRect;

	protected ConversationManager () {}

	public void StartConversation (Conversation conversation) {
		if (!talking) {
			StartCoroutine (DisplayConversation(conversation));
		}
	}

	IEnumerator DisplayConversation(Conversation conversation) {
		talking = true;
		foreach (var conversationLine in conversation.ConversationLines) {
			currentConversationText = "";
			currentConversationLine = conversationLine;
			conversationTextWidth = currentConversationLine.ConversationText.Length * fontSpacing;
			StartCoroutine (BuildCurrentConversationText (currentConversationLine));
			scaledTextureRect = new Rect (
				currentConversationLine.DisplayPic.textureRect.x / currentConversationLine.DisplayPic.texture.width,
				currentConversationLine.DisplayPic.textureRect.y / currentConversationLine.DisplayPic.texture.height,
				currentConversationLine.DisplayPic.textureRect.width / currentConversationLine.DisplayPic.texture.width,
				currentConversationLine.DisplayPic.textureRect.height / currentConversationLine.DisplayPic.texture.height
			);
			yield return new WaitForSeconds(3);
		}
		talking = false;
	}

	IEnumerator BuildCurrentConversationText (ConversationEntry currentLine) {
		buildingText = true;
		float waitTime = Mathf.Min(textSpeed ,(maxTextSpeed / currentLine.ConversationText.Length));
		foreach(char letter in currentLine.ConversationText) {
			currentConversationText += letter;
			yield return new WaitForSeconds(waitTime);
		}
		buildingText = false;
	}

	void OnGUI () {
		GUI.BeginGroup(new Rect(Screen.width / 2 - conversationTextWidth / 2, 50, conversationTextWidth + displayTextureOffset + 10, dialogHeight));
		GUI.Box (new Rect (0, 0, conversationTextWidth + displayTextureOffset + 10, dialogHeight), "");
		// the character name
		GUI.Label (new Rect (displayTextureOffset, 10, conversationTextWidth + 30, 20), currentConversationLine.SpeakingCharacterName);
		// the conversation text
		GUI.Label (new Rect (displayTextureOffset, 30, conversationTextWidth + 30, 20), currentConversationText);
		// the character image
		GUI.DrawTextureWithTexCoords (new Rect (10, 10, 50, 50), currentConversationLine.DisplayPic.texture, scaledTextureRect);
		GUI.EndGroup ();
	}
}
