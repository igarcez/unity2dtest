﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class PlayerSaveState {
	public string Name;
	public int Age;
	public string Faction;
	public string Occupation;
	public int Level;
	public int Health;
	public int Strength;
	public int Magic;
	public int Defense;
	public int Speed;
	public int Damage;
	public int Armor;
	public int NoOfAttacks;
	public string Weapon;
	public Vector2 Position;
	public List<string> Inventory;
}

public static class PlayerSaveStateExtensions {
	public static PlayerSaveState GetPlayerSaveState (this Player input) {
		PlayerSaveState newSaveState = new PlayerSaveState();
		newSaveState.Name = input.Name;
		newSaveState.Age = input.Age;
		newSaveState.Faction = input.Faction;
		newSaveState.Occupation = input.Occupation;
		newSaveState.Level = input.Level;
		newSaveState.Health = input.Health;
		newSaveState.Strength = input.Strength;
		newSaveState.Magic = input.Magic;
		newSaveState.Defense = input.Defense;
		newSaveState.Speed = input.Speed;
		newSaveState.Damage = input.Damage;
		newSaveState.Armor = input.Armor;
		newSaveState.NoOfAttacks = input.NoOfAttacks;
		newSaveState.Weapon = input.Weapon;
		newSaveState.Position = input.Position;
		newSaveState.Inventory = new List<string>();
		
		foreach (var item in input.Inventory)
			newSaveState.Inventory.Add(item.name);
			
		return newSaveState;
	}
	
	public static Player LoadPlayerSaveState(this PlayerSaveState input, Player player) {
		player.Name = input.Name;
		player.Age = input.Age;
		player.Faction = input.Faction;
		player.Occupation = input.Occupation;
		player.Level = input.Level;
		player.Health = input.Health;
		player.Strength = input.Strength;
		player.Magic = input.Magic;
		player.Defense = input.Defense;
		player.Speed = input.Speed;
		player.Damage = input.Damage;
		player.Armor = input.Armor;
		player.NoOfAttacks = input.NoOfAttacks;
		player.Weapon = input.Weapon;
		player.Position = input.Position;
		player.Inventory = new List<InventoryItem>();
		
		foreach (var item in input.Inventory)
			player.Inventory.Add((InventoryItem) Resources.Load("Inventory Items/" + item));
		
		return player;
	}
}
