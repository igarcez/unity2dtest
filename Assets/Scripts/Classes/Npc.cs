﻿using UnityEngine;

public class Npc : MonoBehaviour {
	public string Name;
	[Range(1, 100)]
	public int Age;
	[PopupAttribute("Imperial", "Independant", "Evil")]
	public string Faction;
	[PopupAttribute("Mayor", "Wizard", "Layabout")]
	public string Occupation;
	[Range(1,10)]
	public int Level;
}
