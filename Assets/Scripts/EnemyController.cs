﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
	private BattleManager battleManager;
	public Enemy EnemyProfile;
	Animator enemyAI;
	private bool selected;
	GameObject selectionCircle;
	private ParticleSystem bloodsplatterParticles;

	public BattleManager BattleManager {
		get {
			return battleManager;
		}
		set {
			battleManager = value;
		}
	}

	void Awake () {
		battleManager = GameObject.Find ("BattleManager").GetComponent<BattleManager>();
		bloodsplatterParticles = GetComponentInChildren<ParticleSystem> ();
		if (bloodsplatterParticles == null) {
			Debug.LogError ("No particle system found");
		}
		enemyAI = GetComponent<Animator> ();
		if (enemyAI == null) {
			Debug.LogError ("No AI System found");
		}
	}

	void Update () {
		UpdateAI ();
	}

	void OnMouseDown () {
		if (battleManager.CanSelectEnemy) {
			var selection = !selected;
			battleManager.ClearSelectedEnemy ();
			selected = selection;
			if (selected) {
				selectionCircle = (GameObject) GameObject.Instantiate(battleManager.selectionCircle);
				selectionCircle.transform.parent = transform;
				selectionCircle.transform.localPosition = Vector3.zero;
				selectionCircle.transform.localScale = new Vector3(1, 1, 0);
				StartCoroutine("SpinObject", selectionCircle);
				battleManager.SelectEnemy(this, EnemyProfile.Name);
			}
		}
	}

	public void UpdateAI() {
		if (enemyAI != null && EnemyProfile != null) {
			enemyAI.SetInteger ("EnemyHealth", EnemyProfile.Health);
			enemyAI.SetInteger ("PlayerHealth", GameState.CurrentPlayer.Health);
			enemyAI.SetInteger ("EnemiesInBattle", battleManager.EnemyCount);

		}
	}

	IEnumerator SpinObject(GameObject target) {
		while (true) {
			target.transform.Rotate (0, 0, 180 * Time.deltaTime);
			yield return null;
		}
	}

	public void ClearSelection () {
		if (selected) {
			selected = false;
			if (selectionCircle != null) {
				DestroyObject (selectionCircle);
				StopCoroutine ("SpinObject");
			}
		}
	}

	// used on editor by animation event
	void ShowBloodSplatter() {
		bloodsplatterParticles.Emit(1);
		ClearSelection ();
		if (battleManager != null) {
			battleManager.ClearSelectedEnemy();
		} else {
			Debug.LogError ("No BattleManager");
		}
	}
}
