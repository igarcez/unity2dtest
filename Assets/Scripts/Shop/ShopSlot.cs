﻿using UnityEngine;

public class ShopSlot : MonoBehaviour {
  public InventoryItem Item;
  public ShopManager Manager;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

  public void AddShopItem (InventoryItem item) {
    var spriteRenderer = GetComponent<SpriteRenderer>();
    spriteRenderer.sprite = item.Sprite;
    spriteRenderer.transform.localScale = item.Scale;
    Item = item;
  }

  public void PurchaseItem () {
    GameState.CurrentPlayer.AddInventoryItem (Item);
    Item = null;
    var spriteRenderer = GetComponent<SpriteRenderer>();
    spriteRenderer.sprite = null;
    Manager.ClearSelectedItem ();
  }

  void OnMouseDown () {
    Debug.Log ("shop item clicked" + this);
    if (Item != null) {
      Manager.SetShopSelectedItem (this);
    }
  }
}
