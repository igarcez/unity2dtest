﻿using UnityEngine;

[ExecuteInEditMode]
public class MainMenu : MonoBehaviour {
	
	bool saveAvaiable;
	// Use this for initialization
	void Start () {
		saveAvaiable = GameState.SaveAvaiable;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI () {
		GUILayout.BeginArea (new Rect ((Screen.width / 2) - 100, (Screen.height / 2) - 100, 200, 200));
		if (GUILayout.Button("New Game"))
			NavigationManager.NavigateTo("Home");
		
		GUILayout.Space(50);
		
		if(saveAvaiable) {
			if(GUILayout.Button("Load Game")) {
				GameState.LoadState( () => {
					var lastLocation = PlayerPrefs.GetString("CurrentLocation", "Home");
					NavigationManager.NavigateTo(lastLocation);
				});
			}
		}
		GUILayout.EndArea();		
	}
}
