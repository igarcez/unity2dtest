﻿using UnityEngine;

public class PlayerInventoryDisplay : MonoBehaviour {
  bool displayInventory = false;
  Rect inventoryWindowRect;
  private Vector2 inventoryWindowSize = new Vector2(150, 150);
  Vector2 inventoryItemIconSize = new Vector2(32, 32);

  float offsetX = 6;
  float offsetY = 6;

  void Awake () {
    inventoryWindowRect = new Rect (
      Screen.width / 2 - inventoryWindowSize.x / 2,
      Screen.height - 40 - inventoryWindowSize.y,
      inventoryWindowSize.x,
      inventoryWindowSize.y
    );
  }

  void OnGUI () {
    if (GUI.Button (
      new Rect (
        Screen.width - 40,
        Screen.height - 40,
        40,
        40
      ), "INV"))
    {
      displayInventory = !displayInventory;
    }

    if (displayInventory) {
      inventoryWindowRect = GUI.Window (
        0,
        inventoryWindowRect,
        DisplayInventoryWindow,
        "Inventory"
        );
    }
  }

  void DisplayInventoryWindow (int windowID) {
    var currentX = 0 + offsetX;
    var currentY = 18 + offsetY;
    foreach (var item in GameState.CurrentPlayer.Inventory)
    {
      Rect textcoords = item.Sprite.textureRect;

      textcoords.x /= item.Sprite.texture.width;
      textcoords.y /= item.Sprite.texture.height;
      textcoords.width /= item.Sprite.texture.width;
      textcoords.height /= item.Sprite.texture.height;

      GUI.DrawTextureWithTexCoords (new Rect (
        currentX,
        currentY,
        item.Sprite.textureRect.width,
        item.Sprite.textureRect.height
        ), item.Sprite.texture, textcoords);

      currentX += inventoryItemIconSize.x;
      if (currentX + inventoryItemIconSize.x + offsetX > inventoryWindowSize.x)
      {
        currentX = offsetX;
        currentY += inventoryItemIconSize.y;

        if (currentY + inventoryItemIconSize.y + offsetY > inventoryWindowSize.y)
        {
          return;
        }
      }
    }
  }
}
