﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraLookAt : MonoBehaviour {
	
	public Vector3 cameraTarget = Vector3.zero;
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(cameraTarget);
	}
	
	void OnDrawGizmos () {
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (transform.position, cameraTarget);
	}
}
