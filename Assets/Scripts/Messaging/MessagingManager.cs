﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MessagingManager : Singleton<MessagingManager> {
	private List<Action> subscribers = new List<Action>();
	private List<Action<bool>> uiEventSubscribers = new List<Action<bool>>();
	private List<Action<InventoryItem>> inventorySubscribers = new List<Action<InventoryItem>>();

	protected MessagingManager () {}

	public void Subscribe (Action subscriber) {
		subscribers.Add(subscriber);
		Debug.Log ("Subscriber registered");
	}

	public void Unsubscribe (Action subscriber) {
		subscribers.Remove (subscriber);
		Debug.Log ("Subscriber removed");
	}

	public void ClearAllSubscribers () {
		subscribers.Clear();
	}

	public void Broadcast () {
		Debug.Log ("Broadcast requested, No of Subscribers = " + subscribers.Count);
		foreach (var subscriber in subscribers) {
			subscriber();
		}
	}

	public void SubscribeUIEvent (Action<bool> subscriber) {
		uiEventSubscribers.Add(subscriber);
	}

	public void BroadcastUIEvent (bool uIVisible) {
		foreach (var subscriber in uiEventSubscribers.ToArray()) {
			subscriber(uIVisible);
		}
	}

	public void UnSubscribeUIEvent (Action<bool> subscriber) {
		uiEventSubscribers.Remove(subscriber);
	}

	public void ClearAllUIEventSubscribers () {
		uiEventSubscribers.Clear();
	}

	public void SubscribeInventoryEvent (Action<InventoryItem> subscriber) {
		if (inventorySubscribers != null) {
			inventorySubscribers.Add (subscriber);
		}
	}

	public void BroadcastInventoryEvent (InventoryItem itemInUse) {
		foreach (var subscriber in inventorySubscribers) {
			subscriber(itemInUse);
		}
	}

	public void UnSubscribeInventoryEvent(Action<InventoryItem> subscriber) {
		if (inventorySubscribers != null) {
			inventorySubscribers.Remove (subscriber);
		}
	}

	public void ClearAllInventoryEventSubscribers () {
		if (inventorySubscribers != null) {
			inventorySubscribers.Clear ();
		}
	}
}