﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class SaveSceneOnPlay {
	static SaveSceneOnPlay () {
		EditorApplication.playmodeStateChanged += SaveSceneIfPlaying;
	}
	
	static void SaveSceneIfPlaying () {
		if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying) {
			Debug.Log ("Automatically saving scene (" + EditorApplication.currentScene + ") before entering in play mode");
			EditorApplication.SaveAssets ();
			EditorApplication.SaveScene ();
		}
		
	}
}
