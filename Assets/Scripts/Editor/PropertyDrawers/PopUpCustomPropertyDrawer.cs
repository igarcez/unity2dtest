﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(PopupAttribute))]
public class PopUpCustomPropertyDrawer : PropertyDrawer {
	PopupAttribute popUpAttribute {
		get { return ((PopupAttribute) attribute); }
	}
	
	public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label) {
		if (prop.propertyType != SerializedPropertyType.String) {
			throw new UnityException("Property " + prop + " must be string to use with PopUpAttribute ");
		}
		
		var popupRect = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID(FocusType.Passive), label);
		var currentItem = prop.stringValue;
		var currentIndex = popUpAttribute.value.Length - 1;
		for (; currentIndex >= 0; currentIndex--)
		{
			if(popUpAttribute.value[currentIndex] == currentItem)
				break;
		}
		
		int selectedIndex = EditorGUI.Popup(popupRect, currentIndex, popUpAttribute.value);
		prop.stringValue = selectedIndex < 0 ? "" : popUpAttribute.value[selectedIndex];
	}
}
