﻿using UnityEngine;
using UnityEditor;

public class MyItem : MonoBehaviour {
	[MenuItem ("My Menu/Menu Item 1")]
	static void EnableMyAwesomeFeature () {
		Debug.Log ("I am a leaf on the wind ....");
	}
	
	[MenuItem ("My Menu/Menu Item 1", true)]
	static bool CheckIsObjectIsSelected () {
		return Selection.activeTransform != null;
	}
	
	[MenuItem ("My Menu/Menu Item 2 %g")]
	static void AnotherMenuItem () {
		Debug.Log ("teste with shortcut");
	}
	
	[MenuItem ("CONTEXT/Transform/Move to Center")]
	static void MoveToCenter (MenuCommand command) {
		Transform transform = (Transform) command.context;
		transform.position = Vector3.zero;
		Debug.Log ("Moved object to " + transform.position + " from a context menu");
	}
}
