﻿using UnityEngine;
using UnityEditor;

public class MyEditorWindow : EditorWindow {
	string windowName = "My Editor Window";
	bool groupEnabled;
	bool DisplayToggle = true;
	float Offset = 1.23f;
	
	[MenuItem ("Window/My Window")]
	public static void ShowWindow () {
		EditorWindow.GetWindow(typeof(MyEditorWindow));
	}
	
	void OnGUI () {
		GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
		windowName = EditorGUILayout.TextField ("Window Name", windowName);
		groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
		DisplayToggle = EditorGUILayout.Toggle ("Display Toggle", DisplayToggle);
		Offset = EditorGUILayout.Slider ("Offset Slider", Offset, -3, 3);
		EditorGUILayout.EndToggleGroup();
	}
}
