﻿using UnityEngine;

public class ShopEntry : MonoBehaviour {
  bool canEnterShop;

	void DialogVisible (bool visibility) {
    canEnterShop = visibility;
    MessagingManager.Instance.BroadcastUIEvent (visibility);
  }

  void OnTriggerEnter2D(Collider2D col){
    DialogVisible (true);
  }

  void OnTriggerExit2D(Collider2D col){
    DialogVisible (false);
  }

  void Update() {
    if (canEnterShop && Input.GetKeyDown (KeyCode.UpArrow)) {
        Debug.Log (this.tag);
      if (NavigationManager.CanNavigate (this.tag)) {
        NavigationManager.NavigateTo (this.tag);
      }
    }
  }

  void OnGUI () {
    if (canEnterShop) {
      GUI.BeginGroup (
        new Rect (
          Screen.width / 2 - 150,
          50,
          300,
          50
          )
        );

      GUI.Box ( new Rect (0,0,300,250), "");
      GUI.Label (
        new Rect (15, 10, 300, 68),
        "Shop (press up to enter)"
        );
      GUI.EndGroup ();
    }
  }
}
